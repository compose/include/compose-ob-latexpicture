;; [[file:ob-latexpicture.org::*Literate programming][Literate programming:1]]
;; This is a literate programming file. The source of this file is ob-latexpicture.org.
;; Literate programming:1 ends here

;; [[file:ob-latexpicture.org::*Definition of =latexpicture= =lang=][Definition of =latexpicture= =lang=:1]]
(require 'ob)

(add-to-list 'org-src-lang-modes '("latexpicture" . latex))

(defconst org-babel-header-args:latexpicture
  '((border         . :any)
    (fit            . :any)
    (imagemagick    . ((nil t)))
    (iminoptions    . :any)
    (imoutoptions   . :any)
    (packages       . :any)
    (pdfheight      . :any)
    (pdfpng         . :any)
    (pdfwidth       . :any)
    (headers        . :any)
    (packages         . :any)
    (buffer         . ((yes no)))
    (usemacros      . ((nil t)))
                                        ; (ox-html-only   . ((nil t)))
    )
  "latexpicture-specific header arguments.")

(defvar org-babel-default-header-args:latexpicture
  '((:results . "output graphics file")
    (:exports . "results")
    (:usemacros . nil)))

(setq org-babel-latex-latexpicture-packages '("[usenames]{color}" "{tikz}" "{color}" "{listings}" "{braket}" "{amsmath}" "{amsfonts}" "{amsopn}" "[algo2e,ruled,vlined]{algorithm2e}"))

(defcustom org-babel-latexpicture-preamble
  (lambda (_)
    "\\documentclass[preview]{standalone}
  ")
  "Closure which evaluates at runtime to the LaTeX preamble.

  It takes 1 argument which is the parameters of the source block."
  :group 'org-babel
  :type 'function)



(defun org-babel-execute:latexpicture (body params)
  "Execute a block of Latex Picture code with Babel.

      This function is called by `org-babel-execute-src-block'.

   The aim of this function is to provide the opportunity to
   generate an svg picture that will be embedded in the generated
   document. This is the behaviour by default for html export as
   soon as a file name (through the :file header) is provided.
   For other backends, the body of the code is left as is unless
   ox-html-only is disabled (:ox-html-only nil)
  "
  (setq body (org-babel-expand-body:latex body params))
  (print "backend:")
  (print org-export-current-backend)
  ;; (if (not (cdr (assq :ox-html-only params))) (print "not ox-html-only"))
  ;; (if (string= org-export-current-backend "html") (print "html backend"))
  (if (and (cdr (assq :file params))) ;; (or (not (cdr (assq :ox-html-only params))) (string= org-export-current-backend "html")))
      (let* ((out-file (cdr (assq :file params)))
             (extension (file-name-extension out-file))
             (tex-file (org-babel-temp-file "latex-" ".tex"))
             (border (cdr (assq :border params)))
             (imagemagick (cdr (assq :imagemagick params)))
             (im-in-options (cdr (assq :iminoptions params)))
             (im-out-options (cdr (assq :imoutoptions params)))
             (fit (or (cdr (assq :fit params)) border))
             (usemacros (cdr (assq :usemacros params)))
             (height (and fit (cdr (assq :pdfheight params))))
             (width (and fit (cdr (assq :pdfwidth params))))
             ;; (scale (cdr (assq :scale params)))
             (headers (cdr (assq :headers params)))
             (in-buffer (not (string= "no" (cdr (assq :buffer params)))))
             (org-latex-packages-alist
              (append (cdr (assq :packages params)) org-latex-packages-alist)))
        (print "HELLO WORLD")
        (print extension)
        (print out-file)
        (print imagemagick)
        (print fit)
        (print usemacros)
        (print width)
        ;; (print scale)
        (print headers)
        (print "BYE WORLD")
        (cond
         ((and (string-suffix-p ".png" out-file) (not imagemagick))
          ((print "PNG SUFFIX")
           (let ((org-format-latex-header
                  (concat org-format-latex-header "\n"
                          (mapconcat #'identity headers "\n"))))
             (org-create-formula-image
              body out-file org-format-latex-options in-buffer))))
         ((string= "svg" extension)
          (with-temp-file tex-file
            (print "SVG EXTENSION for tex-file:")
            (print tex-file)
            (insert (concat (funcall org-babel-latexpicture-preamble params)
                            ;; (if fit "\n\\usepackage[active, tightpage]{preview}\n" "")
                            ;; (if border (format "\\setlength{\\PreviewBorder}{%s}" border) "")
                            ;; (if height (concat "\n" (format "\\pageheight %s" height)) "")
                            ;; (if width  (concat "\n" (format "\\pagewidth %s" width))   "")
                            (mapconcat (lambda (pkg)
                                         (concat "\\usepackage"
                                                 pkg))
                                       org-babel-latex-latexpicture-packages
                                       "\n")
                            (if headers
                                (concat "\n"
                                        (if (listp headers)
                                            (mapconcat #'identity headers "\n")
                                          headers) "\n")
                              "")
                            (if usemacros "\n\\include{latex-macros}\n" "")
                            (funcall org-babel-latex-begin-env params)
                            ;; (if scale (format "\n\\scalebox{%s}{\n" scale)   "")
                            body
                            ;; (if scale "\n}\n" "")
                            (funcall org-babel-latex-end-env params))))
          (let ((tmp-pdf (org-babel-latex-tex-to-pdf tex-file)))
            (let* ((log-buf (get-buffer-create "*Org Babel LaTeX -> SVG Output*"))
                   (err-msg "org babel latex failed")
                   (img-out (org-compile-file
                             tmp-pdf
                             (list org-babel-latex-pdf-svg-process)
                             extension err-msg log-buf)))
              (print (format "org-compile-file tmp-pdf=%s extension=%s" tmp-pdf extension))
              (print org-babel-latex-pdf-svg-process)
              (print (format "img-out: %s, out-file: %s" img-out out-file))
              (shell-command (format "mv %s %s" img-out out-file)))))
         ((string-suffix-p ".tikz" out-file)
          ((print "TIKZ SUFFIX")
           (when (file-exists-p out-file) (delete-file out-file))
           (with-temp-file out-file
             (insert body))))
         ;; ((and (or (string= "html" extension) (string= "svg" extension))
         ((and (string= "html" extension)
               (executable-find org-babel-latex-htlatex))
          ;; TODO: this is a very different way of generating the
          ;; frame latex document than in the pdf case.  Ideally, both
          ;; would be unified.  This would prevent bugs creeping in
          ;; such as the one fixed on Aug 16 2014 whereby :headers was
          ;; not included in the SVG/HTML case.
          ((print "HTML" extension)
           (with-temp-file tex-file
             (insert (concat
                      "\\documentclass[preview]{standalone}
      \\def\\pgfsysdriver{pgfsys-tex4ht.def}
      "
                      (mapconcat (lambda (pkg)
                                   (concat "\\usepackage" pkg))
                                 org-babel-latex-htlatex-packages
                                 "\n")
                      (if headers
                          (concat "\n"
                                  (if (listp headers)
                                      (mapconcat #'identity headers "\n")
                                    headers) "\n")
                        "")
                      "\\begin{document}"
                      body
                      "\\end{document}")))
           (when (file-exists-p out-file) (delete-file out-file))
           (let ((default-directory (file-name-directory tex-file)))
             (shell-command (format "%s %s" org-babel-latex-htlatex tex-file)))
           (cond
            ((file-exists-p (concat (file-name-sans-extension tex-file) "-1.svg"))
             (if (string-suffix-p ".svg" out-file)
                 (progn
                   (shell-command "pwd")
                   (shell-command (format "mv %s %s"
                                          (concat (file-name-sans-extension tex-file) "-1.svg")
                                          out-file)))
               (error "SVG file produced but HTML file requested")))
            ((file-exists-p (concat (file-name-sans-extension tex-file) ".html"))
             (if (string-suffix-p ".html" out-file)
                 (shell-command "mv %s %s"
                                (concat (file-name-sans-extension tex-file)
                                        ".html")
                                out-file)
               (error "HTML file produced but SVG file requested"))))))
         ((or (string= "pdf" extension) imagemagick)
          (with-temp-file tex-file
            (require 'ox-latex)
            (print "PDF")
            (insert
             (org-latex-guess-inputenc
              (org-splice-latex-header
               org-format-latex-header
               (delq
                nil
                (mapcar
                 (lambda (el)
                   (unless (and (listp el) (string= "hyperref" (cadr el)))
                     el))
                 org-latex-default-packages-alist))
               org-latex-packages-alist
               nil))
             (if fit "\n\\usepackage[active, tightpage]{preview}\n" "")
             (if border (format "\\setlength{\\PreviewBorder}{%s}" border) "")
             (if height (concat "\n" (format "\\pdfpageheight %s" height)) "")
             (if width  (concat "\n" (format "\\pdfpagewidth %s" width))   "")
             (if headers
                 (concat "\n"
                         (if (listp headers)
                             (mapconcat #'identity headers "\n")
                           headers) "\n")
               "")
             (if fit
                 (concat "\n\\begin{document}\n\\begin{preview}\n" body
                         "\n\\end{preview}\n\\end{document}\n")
               (concat "\n\\begin{document}\n" body "\n\\end{document}\n"))))
          (when (file-exists-p out-file) (delete-file out-file))
          (let ((transient-pdf-file (org-babel-latex-tex-to-pdf tex-file)))
            (cond
             ((string= "pdf" extension)
              (rename-file transient-pdf-file out-file))
             (imagemagick
              (org-babel-latex-convert-pdf
               transient-pdf-file out-file im-in-options im-out-options)
              (when (file-exists-p transient-pdf-file)
                (delete-file transient-pdf-file)))
             (t
              (error "Can not create %s files, please specify a .png or .pdf file or try the :imagemagick header argument"
                     extension))))))
        nil) ;; signal that output has already been written to file
    body))

(defun org-babel-execute:oldlatexpicture (body params)
  "Execute a block of Latex Picture code with Babel.

      This function is called by `org-babel-execute-src-block'.

   The aim of this function is to provide the opportunity to
   generate an svg picture that will be embedded in the generated
   document. This is the behaviour by default for html export as
   soon as a file name (through the :file header) is provided.
   For other backends, the body of the code is left as is unless
   ox-html-only is disabled (:ox-html-only nil)
  "
  (setq body (org-babel-expand-body:latex body params))
  (print "backend:")
  (print org-export-current-backend)
  (if (and (cdr (assq :file params)))
      (let* ((out-file (cdr (assq :file params)))
             (extension (file-name-extension out-file))
             (tex-file (org-babel-temp-file "latex-" ".tex"))
             (border (cdr (assq :border params)))
             (imagemagick (cdr (assq :imagemagick params)))
             (im-in-options (cdr (assq :iminoptions params)))
             (im-out-options (cdr (assq :imoutoptions params)))
             (fit (or (cdr (assq :fit params)) border))
             (usemacros (cdr (assq :usemacros params)))
             (height (and fit (cdr (assq :pdfheight params))))
             (width (and fit (cdr (assq :pdfwidth params))))
             (headers (cdr (assq :headers params)))
             (in-buffer (not (string= "no" (cdr (assq :buffer params)))))
             (org-latex-packages-alist
              (append (cdr (assq :packages params)) org-latex-packages-alist)))
        (cond
         ((and (string-suffix-p ".png" out-file) (not imagemagick))
          ((print "PNG SUFFIX")
           (let ((org-format-latex-header
                  (concat org-format-latex-header "\n"
                          (mapconcat #'identity headers "\n"))))
             (org-create-formula-image
              body out-file org-format-latex-options in-buffer))))
         ((string= "svg" extension)
          (with-temp-file tex-file
            (insert (concat (funcall org-babel-latexpicture-preamble params)
                            (mapconcat (lambda (pkg)
                                         (concat "\\usepackage"
                                                 pkg))
                                       org-babel-latex-latexpicture-packages
                                       "\n")
                            (if headers
                                (concat "\n"
                                        (if (listp headers)
                                            (mapconcat #'identity headers "\n")
                                          headers) "\n")
                              "")
                            (if usemacros "\n\\include{latex-macros}\n" "")
                            (funcall org-babel-latex-begin-env params)
                            body
                            (funcall org-babel-latex-end-env params))))
          (let ((tmp-pdf (org-babel-latex-tex-to-pdf tex-file)))
            (let* ((log-buf (get-buffer-create "*Org Babel LaTeX -> SVG Output*"))
                   (err-msg "org babel latex failed")
                   (img-out (org-compile-file
                             tmp-pdf
                             (list org-babel-latex-pdf-svg-process)
                             extension err-msg log-buf)))
              (print (format "org-compile-file tmp-pdf=%s extension=%s" tmp-pdf extension))
              (print org-babel-latex-pdf-svg-process)
              (print (format "img-out: %s, out-file: %s" img-out out-file))
              (shell-command (format "mv %s %s" img-out out-file)))))
         ((string-suffix-p ".tikz" out-file)
          (when (file-exists-p out-file) (delete-file out-file))
          (with-temp-file out-file
            (insert body))))
        ;; ((and (or (string= "html" extension) (string= "svg" extension))
        ((and (string= "html" extension)
              (executable-find org-babel-latex-htlatex))
         ;; TODO: this is a very different way of generating the
         ;; frame latex document than in the pdf case.  Ideally, both
         ;; would be unified.  This would prevent bugs creeping in
         ;; such as the one fixed on Aug 16 2014 whereby :headers was
         ;; not included in the SVG/HTML case.
         ((print "HTML" extension)
          (with-temp-file tex-file
            (insert (concat
                     "\\documentclass[preview]{standalone}
      \\def\\pgfsysdriver{pgfsys-tex4ht.def}
      "
                     (mapconcat (lambda (pkg)
                                  (concat "\\usepackage" pkg))
                                org-babel-latex-htlatex-packages
                                "\n")
                     (if headers
                         (concat "\n"
                                 (if (listp headers)
                                     (mapconcat #'identity headers "\n")
                                   headers) "\n")
                       "")
                     "\\begin{document}"
                     body
                     "\\end{document}")))
          (when (file-exists-p out-file) (delete-file out-file))
          (let ((default-directory (file-name-directory tex-file)))
            (shell-command (format "%s %s" org-babel-latex-htlatex tex-file)))
          (cond
           ((file-exists-p (concat (file-name-sans-extension tex-file) "-1.svg"))
            (if (string-suffix-p ".svg" out-file)
                (progn
                  (shell-command "pwd")
                  (shell-command (format "mv %s %s"
                                         (concat (file-name-sans-extension tex-file) "-1.svg")
                                         out-file)))
              (error "SVG file produced but HTML file requested")))
           ((file-exists-p (concat (file-name-sans-extension tex-file) ".html"))
            (if (string-suffix-p ".html" out-file)
                (shell-command "mv %s %s"
                               (concat (file-name-sans-extension tex-file)
                                       ".html")
                               out-file)
              (error "HTML file produced but SVG file requested"))))))
        ((or (string= "pdf" extension) imagemagick)
         (with-temp-file tex-file
           (require 'ox-latex)
           (print "PDF")
           (insert
            (org-latex-guess-inputenc
             (org-splice-latex-header
              org-format-latex-header
              (delq
               nil
               (mapcar
                (lambda (el)
                  (unless (and (listp el) (string= "hyperref" (cadr el)))
                    el))
                org-latex-default-packages-alist))
              org-latex-packages-alist
              nil))
            (if fit "\n\\usepackage[active, tightpage]{preview}\n" "")
            (if border (format "\\setlength{\\PreviewBorder}{%s}" border) "")
            (if height (concat "\n" (format "\\pdfpageheight %s" height)) "")
            (if width  (concat "\n" (format "\\pdfpagewidth %s" width))   "")
            (if headers
                (concat "\n"
                        (if (listp headers)
                            (mapconcat #'identity headers "\n")
                          headers) "\n")
              "")
            (if fit
                (concat "\n\\begin{document}\n\\begin{preview}\n" body
                        "\n\\end{preview}\n\\end{document}\n")
              (concat "\n\\begin{document}\n" body "\n\\end{document}\n"))))
         (when (file-exists-p out-file) (delete-file out-file))
         (let ((transient-pdf-file (org-babel-latex-tex-to-pdf tex-file)))
           (cond
            ((string= "pdf" extension)
             (rename-file transient-pdf-file out-file))
            (imagemagick
             (org-babel-latex-convert-pdf
              transient-pdf-file out-file im-in-options im-out-options)
             (when (file-exists-p transient-pdf-file)
               (delete-file transient-pdf-file)))
            (t
             (error "Can not create %s files, please specify a .png or .pdf file or try the :imagemagick header argument"
                    extension))))))
    nil) ;; signal that output has already been written to file
  body)
;; Definition of =latexpicture= =lang=:1 ends here

;; [[file:ob-latexpicture.org::*Provides][Provides:1]]
(provide 'ob-latexpicture)
;; Provides:1 ends here
